This folder is used as a destination for the database backup so that its data can be acessed in the host.

To create the backup, run the following command once the container is running:

```
docker exec docker_db_1 cp jpadb.mv.db ../data
```