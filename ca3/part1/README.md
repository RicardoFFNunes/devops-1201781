# Class Assignment 3 - Part 1

## Virtualization with Vagrant

This is the README for the third class assignment.

This class assignment subject is **Virtualization with Vagrant**.

On this first part of the third assignment, the goal is to practice with VirtualBox using the same projects from the previous assignments but now inside a 
VirtualBox VM with Ubuntu.

## 1. Analysis, Design and Implementation

### Virtualization

Virtualization is the process of running a virtual instance of a computer system in a layer abstracted from the actual hardware.

#### Hypervisor

A hypervisor is computer software, firmware or hardware that creates and runs virtual machines. 

A computer on which a hypervisor runs one or more virtual machines is called a **host machine**, and each virtual machine is called a **guest machine**.

Hypervisors can be divided into two types:

- Type 1, native or bare-metal hypervisors: These run directly on the host computer’s hardware to control the hardware resources and to manage guest operating systems.

- Type 2, hosted hypervisors: These run within a conventional operating system environment.  A guest operating system runs as a process on the host. This type also abstracts guest operating systems from the host operating system.

The following image ilustrates those two types:

![image](./prtsc/general/hypervisor_types.png)

The chosen hypervisor for the assigment was **VirtualBox**, which is a type 2 hypervisor.

VirtualBox is an OpenSource hypervisor, available for multiple operating systems.

## Issues

Optionally, you can create issues on Bitbucket to track the development of this class assignment. They could represent software features, story leads, user stories, etc.

For the development of this assigment, 11 issues can be created:

1. Download **VirtualBox**;
2. Configure the Virtual Machine with Ubuntu;
3. Run your Virtual Machine;
4. Update Ubuntu's applications;
5. Install **Git**;
6. Install **JDK**;
7. Install **Gradle**;
8. Clone the spring boot tutorial basic project (ca1) and the gradle tutorial basic project (ca2-part1);
10. Build and execute the spring boot tutorial basic project;
11. Change, build and execute the gradle tutorial basic;
12. At the end of the part 1 of this assignment mark the repository with the tag ca3-part1.

## Download VirtualBox (Issue 1)

You can download the latest VirtualBox version here:

https://www.virtualbox.org/

## Configure the Virtual Machine with Ubuntu (Issue 2)

To be able to access the guest OS, it's necessary to configure the network interface of the machine.
We should connect the (image)[https://help.ubuntu.com/community/Installation/MinimalCD] (ISO) with the Ubuntu 18.04 minimal installation media.
As the several applications that we will run, are going to broadcast to `localhost`, there needs to be a way for the host machine to access the guest.

- The VM needs 2048 MB RAM.
- Set Network Adapter 1 as Nat
- Set Network Adapter 2 as Host-only Adapter (vboxnet0).

To do the latest, we need to add to the guest a `Host-only Adapter`. To do that:

- From main menu select File -> Host Network Manager
- Click create button. A new Host-only network will be created and added to the list.

We have now enabled Network Adapter 2 as Host-only Adapter (vboxnet0).

We should check the IP address range of this network. 

In this case it was 192.168.56.1/24.

We should select an IP in this range for the second adapter of our VM.

In this case I used 192.168.99.1/24.

## Run your Virtual Machine (Issue 3)

To run your virtual machine just press the "Start" button:

![image](./prtsc/general/start.png)

## Update Ubuntu's applications (Issue 4)

You can now continue the setup.

First, update the packages repositories:

```
sudo apt update
```

Then, install the network tools:

```
sudo apt install net-tools
```

Now, edit the network configuration file to setup the IP:

```
sudo nano /etc/netplan/01-netcfg.yaml
```

![image](./prtsc/general/netplan.png)

Make sure the contents of the file are similar to the following (in this case we are setting the IP of the second adapter as 192.168.99.5):

## Install Git (Issue 5)

To install **Git** you can enter the following command:

```
sudo apt install git
```

## Install JDK (Issue 6)

To install **JDK** you can enter the following command:

```
sudo apt install openjdk-8-jdk-headless
```

## Install Gradle (Issue 7)

To install **Gradle** you can enter the following command:

```
sudo apt install gradle
```

## Clone the spring boot tutorial basic project (ca1) and the gradle tutorial basic (ca2-part1) (Issue 8)

To clone the repository which contains both projects, insert the following command inside the directory that you want the repository to be cloned to:

```
git clone https://bitbucket.org/RicardoNunes230/devops-20-21-1201781.git
```

## Build and execute the spring boot tutorial basic project (Issue 9)

Now, in order to build and execute the spring boot tutorial basic project, we need to change our current directory, like in the following image:

![image](./prtsc/ca1_spring_tutorial_basic_project/change_directory_ca1_tut-basic.png)

Then you need to run the application with the following command:

```
./mvnw spring-boot:run
```

The following should appear in your terminal:

![image](./prtsc/ca1_spring_tutorial_basic_project/spring_boot_run.png)

And this should stay in your terminal when the servlet is online:

![image](./prtsc/ca1_spring_tutorial_basic_project/servlet_online.png)

You can now see your webpage in your host computer by entering the corresponding application link in your web browser following this model:

```
<Your_Server_Host_IP_Address>:<Your_Application_Port>
```

In my case it was:

```
192.168.99.5:8080
```

You should see this in your webpage:

![image](./prtsc/ca1_spring_tutorial_basic_project/web_app.png)

You are now ready to close your servlet. To do so press the following keys in your terminal (excluding the plus sign):

```
CTRL + C
```

The following should appear:

![image](./prtsc/ca1_spring_tutorial_basic_project/build_sucessfull.png)

## Change, build and execute the gradle tutorial basic project (Issue 10)

Now, in order to build and execute the gradle tutorial basic project, we need to change our current directory, like in the following image:

![image](./prtsc/ca2_part1_gradle_basic_demo/change_directory_gradle_basic_demo.png)

Then you need to build the application using:

```
gradle build
```

Like this:

![image](./prtsc/ca2_part1_gradle_basic_demo/build_sucessfull.png)

Then we need to run the server application with by entering the following command:

```
gradle runServer
```

You should see the following in your terminal:

![image](./prtsc/ca2_part1_gradle_basic_demo/runServer.png)

Now, in order to test the client part of the project, we need to run the client-side applications in our host OS, since Ubuntu does not have a Graphical Unit Interface.

Since the server is now being hosted in the Virtual Machine, we need to create a new task for the client side of the application in order to execute said task and
connect to the server IP instead of the localhost.

In order to do so, we need to add the following task in our "build.gradle" inside our repository located in our host OS. So we need to go there:

![image](./prtsc/ca2_part1_gradle_basic_demo/change_directory_gradle_basic_demo_2.png)

And then add our "runClientVM" task in our "build.gradle":

```
task runClientVM(type: JavaExec, dependsOn: classes) {
	group = "DevOps"
	description = "Launches a chat client that connects to a server on 192.168.99.5:59001"

    classpath = sourceSets.main.runtimeClasspath

    main = 'basic_demo.ChatClientApp'

    args '192.168.99.5', '59001'
}
```

It should look like this:

![image](./prtsc/ca2_part1_gradle_basic_demo/create_runClientVM_task.png)

Then you need to build the application using:

```
gradle build
```

Like this:

![image](./prtsc/ca2_part1_gradle_basic_demo/gradle_build_new_task.png)

Finally you can run the client-side application:

![image](./prtsc/ca2_part1_gradle_basic_demo/runClientVM.png)

And test it:

![image](./prtsc/ca2_part1_gradle_basic_demo/all_working.png)

## At the end of the part 1 of this assignment mark the repository with the tag ca3-part1 (Issue 11)

With the conclusion of this class assignment task, you can now add the "ca3-part1" tag to your repository.

In order to do that you can run the following commands in your local repository:

```
git tag -a ca3-part1 -m "End of ca3-part1 assignment"

git push origin ca3-part1
```