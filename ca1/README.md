# Class Assignment 1 Report

## Version Control with Git

This is the README for the first class assignment regarding Version Control with Git, which is a distributed version control technology.

The main goal of this assignment is to implement a simple scenario illustrating a Git workflow.

To illustrate this scenario it was proposed to add the [Tutorial React.js and Spring Data REST](https://github.com/spring-guides/tut-react-and-spring-data-rest) application to our repository and make changes to it using the version of the application inside the "basic" folder.

For each improvement or new feature we will have a new branch with the name of the developed feature or related bug fix.

For each stable version, a tag will be added so that we can easily identify them.

## 1. Analysis, Design and Implementation

## BitBucket repository

The first step is to create a repository.

As proposed by the teacher, the repository hosting service chosen to implement was BitBucket, a web-based version control repository made by Atlassian.

After creating the repository, which in this case was named 'devops-20-21-1201781', the following step is to initialize a directory on the local machine to be connected to the online repository using Git.

We can do this in two ways (A and B):

## A) You can turn a local directory into a Git repository:

First it is necessary to go to the project directory that you want to start to perform version control with Git.

You can do it by using the command:

```
cd <path to the intended directory> 
```

Then use the following command:

```
git init
```

This command creates a new subdirectory named ".git", inside your local directory, that contains a Git repository skeleton.

The next step is to connect your local project directory with the Bitbucket repository.

```
git remote add origin https://linkToRepository/UserName/RepositoryName.git
```

This command will create a new remote called origin at __linkToRepository/UserName/RepositoryName.git__ . 

Then you should synchronise your local branch "master" with the remote name origin, by using the command:

```
git push -u origin master
```

With this command your local branch "master" will be sent to the remote repository and it will set it to be the default "upstream" branch for this particular branch. 

## B) You can clone an existing Git repository:

To clone an existing Git repository, you first need to navigate to the destination directory you want to clone the repository.

You can do it by using the command:

```
cd <path to the intended directory> 
```

After this, you should execute:

```
git clone https://linkToRepository/UserName/RepositoryName.git
```

This will create a folder named __RepositoryName__ inside the chosen directory. This folder will contain a clone of the Bitbucket repository.

## Cloning the tut-basic folder

Now that we have a local repository connected to the Bitbucket repository, we need to get the proposed application project.

First, create a folder named "ca1" inside your local repository.

You can do it by using the command:

```
mkdir ca1
```

Then clone the tut-basic folder that is on the [Tutorial React.js and Spring Data REST](https://github.com/spring-guides/tut-react-and-spring-data-rest) to "ca1" directory.

You should also add a README file to document the assignment, similar to this one.

## Adding the README file

You can add the README file by doing:

```
echo 'This is the README file' > README
```

The file will be automatically created with 'This is the README file' line inside.

## Adding files to your repository

After downloading the application and adding it to the local directory, it's necessary to start version-controlling the files added.


First, these files have to be staged using the command:

```
git add -A
```

Now, to verify in which state you have your files use:

```
git status
```

With this command you can see the files that aren't in the staging area yet (will appear as "Changes not staged for commit"), and the files on the staging area (it will appear under the "Changes to be committed" heading).

After the commit and push your status will be "up-to-date".

Commit the files using the command:

```
git commit -m "Initial commit"
```

A message can be added to the commit using the flag __-m__ and inserting the message afterwards inside the quotation marks.

When all the files have been added and committed, they can be pushed to the repository using the following command:

```
git push origin master
```

You can now check your files state. It should be "up-to-date".


## Issues

Optionally, you can create issues on Bitbucket to track the development of your application. They could represent software features, story leads, user stories, etc.

During the development of this assigment 11 issues could be created:

1. Tag the initial version as "v1.2.0"
2. Create a new branch named "email-field"
3. Add support for email field
4. Add unit tests for testing the creation of employees and their attributes validation
5. Debug the server and clients parts of the solution
6. Merge email-field branch with master and tag it as "v1.3.0"
7. Create a new branch for bug fixing called "fix-invalid-email"
8. Add validation to only accept emails with the "@" sign
9. Debug the server and clients parts of the solution
10. Merge the branch to master with tag change in the minor number as "v1.3.1"
11. At the end, mark the repository with tag "ca1"

## Tagging

When important "checkpoints" are reached in the development of the application, it's fundamental to tag its commit using a tag.

Tags usually follow a version structure like "major.minor.revision".

The initial version of this assignment will be tagged "v1.2.0".

You can do it with the following command:

```
git tag -a v1.2.0 -m "Initial version tag"
```

The tag will be commented with "Initial version tag".

After tagging, it's necessary to push the tag to the repository.

You can do it with the following command:

```
git push origin v1.2.0
```

At this point you should have on your repository the folder ca1 with the README file and the tut-basic folder inside.

The last commit should also be associated with the tag "v1.2.0".

## Branching

To implement new features to the code, branching should be used to develop over a stable build of the application.

To add the necessary email field to the application, a branch __email-field__ will be added to the repository.

To create the branch use the following command:

```
git branch email-field
```

After the branch is created, it's necessary to checkout from your current branch to the new one using the command:

```
git checkout email-field
```

## Adding an email field to the application

In order to implement this new feature, changes to the constructor of the "Employee.java" were made and the addition of getter and setter methods for this field, along with changes on "DatabaseLoader.java" and "app.js".

We can now add all the modified files, commit and push the "email-field" branch to the repository:

```
git add .

git commit -m "Added support for email field"

git push origin email-field
```

Note: "git add ." adds all the created/deleted/modified files to the staging area.

Along with this changes, it was necessary to test this new feature.

In order to fulfill the next requirement, tests to all Employee attributes were made. Now it will not accept null or empty values.

Finnaly, both the server and client parts of the solution were thoroughly debbuged.

And once again we add, commit and push our branch to the repository:

```
git add .

git commit -m "Added unit tests for Employee creation and their attributes"

git push origin email-field
```

## Merging and tagging the new feature branch

Now it is necessary to merge the "email-field" branch into the "master" and push it to the repository, using the following commands:

```
git checkout master

git merge email-field
```

In the first command we checked out to the "master" branch. Then we merged the "email-field" branch into it.

Now we need to push the changes to the repository:

```
git push origin master
```

Then we need to tag the new feature in the "master" branch:

```
git tag -a v1.3.0 -m "Implemented 'Email' in tut-basic app (ca1)"
```

Finally, we push the tag:

```
git push origin v1.3.0
```

## Validating email

After this, we needed to implement a bug fix in the application with a validator that makes sure that the inserted email is a valid one (with an @ sign in it).

To implement this bug fix a new branch was created with the name "fix-invalid-email".

```
git checkout -b fix-invalid-email
```

This command created a new branch called "fix-invalid-email" and automatically checked out to it.

When the fix is implemented, we need to add, commit and push our branch to the repository:

```
git add .

git commit -m "Added email format validation"

git push origin fix-invalid-email
```

When the fix is tested, the code should be added, committed and pushed. The branch should also be merged with "master".

```
git add .

git commit -m "Added unit tests for email validation"

git push origin fix-invalid-email

git checkout master

git merge fix-invalid-email
```

After this merge a new version of the application is ready and stable, so a tag should be created and pushed to inform of this.

```
git tag -a v1.3.1 -m 'Fixed invalid email bug'

git push v1.3.1
```

## Final step

After finalizing the assignment and completing the README file, the repository should be market with a new tag "ca1".


## 2. Analysis of an Alternative

The chosen alternative was Plastic SCM.

Plastic SCM allows you to work centralized and distributed. 

Working centralized means that you are sharing the server "live" with everyone. The central server keeps the history of changes from which everyone requests the latest version of the work and pushes the latest changes to. The big disadvantage is if the central versioning server fails, all will crash, but it's way lighter.

Working distributed means that everyone has a local copy of the entire work’s history. This means that it is not necessary to be online to change revisions or add changes to the work.

Now, about Plastic SCM, the major advantages compared to Git are:

- Better GUI, with easier to understand branch and merges visualization;
- Easier to use, even by non-experienced users;
- Very fast, even when handling big files;
- Supports centralized workflows.

Now, let's talk a little more about branches:

Regarding Git, the branch is just a pointer to a given commit.

However in Plastic SCM you have real branches, they are changeset containers. This means that all your work regarding a specific branch will belong to those branch changesets.

About the disadvantages:

- Can't preview the repository in the cloud dashboard;
- It can be harder to install when compared to Git;
- Configuration of the server/client much harder than just cloning a repository like in Git;
- Plastic SCM Cloud is not as resourceful as Bitbucket/GitHub;
- Is not available to Android or iPhone/iPad.

## 3. Implementation of the Alternative

The first thing you need to do is download the Plastic SCM Cloud Edition [here](https://www.plasticscm.com/plasticscm-cloud-edition).

## Configure

After you download it, it will open the first window for you to login. Enter your credentials and login.

Then you need to choose between Plastic For Developers (Plastic GUI) or GLUON graphical unit interface. Choose the first one.

![image](./plasticscm_prtsc/configuracao_escolherGUI.png)

After that you can choose to work centralized or distributed.

![image](./plasticscm_prtsc/configuracao_passo1.png)

When you don't have a local folder, you can simply choose your cloud repository and choose to automatically create the folder on your machine.

## Getting started

Now, first thing you should do is copy the tut-basic folder to the new directory that you sync with the cloud.

After copying it you should submit the changes.

## Adding files to your repository

Make sure that you are in the intended folder and then run the command:

```
cm add -R *
```

This will add all the new content to version control, that is like the staging area for Git.

Then you need to checkin the changes:

```
cm ci --all -c "message"
```

The __-c__ flag enables you to write a customized comment to go along the changeset.

At plastic SCM "checkin" is the same as "commit" for Git.

All your changes will go to the changeset, that will have an automatic number by default. 

Note that the main branch is actually called "main" in Plastic SCM instead of "master" as in Git.

Now you just need to push your changesets to your remote cloud:

```
cm push <origin branch> <destination repository>
```

You can check all the revisions on your changesets by running:

```
cm log
```

## Labelling

Like you did with tag in Git you should do it with labels in Plastic SCM.

Now that you have the original app version on your repository you should add the label "v1.2.0":

```
cm label create v1.2.0 -c "Original version of the application"
```

## Branching

Same thing with the branches, you should create new branches for each new feature.

Let's start with the "email-field" branch:

```
cm br create main/email-field
```

With this command, you create the "email-field" branch from the last changeset in the "main" branch.

Now you have the branch, you need to navigate there.

Like you have the Git "checkout", here you have "switch":

```
cm switch main/email-field 
```

## New features

The changes in the application will be the same that you did previously.

The only difference is about the commands used to "add", "checkin", "label", create branch action, "switch", "merge" and "push" in Plastic SCM when compared to "add", "commit", "tag", create branch action, "checkout", "merge" and "push" in Git. 

## Merging the new feature branch

First, you need to switch to the "main" branch, or the one that you want to be the destination of your merge.

Then you should enter the following command:

```
cm merge main/email-field --merge 
```

After the merge you need to add, checkin and push the changes from the merge. Don't forget to label your stable version accordingly in "main".

## To finish

At the end you should have two new branches ("email-field" and "fix-email-field") and they should be merged into "main" and all should be functional.

Your last label should be "v1.3.1".

Unfortunately, you cannot see your changes in your Plastic SCM Cloud dashboard.

However the final result should be like this:

![image](./plasticscm_prtsc/resultado_final.png)
_Printscreen from Plastic SCM GUI_