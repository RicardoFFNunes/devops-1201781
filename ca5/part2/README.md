# Class Assignment 5

## CI/CD Pipelines with Jenkins

This is the README for the part two of the fifth class assignment.

This class assignment subject is **CI/CD Pipelines with Jenkins**.

## 1. Analysis, Design and Implementation

### Continuous Integration/Continuous Deployment

### Issues

Optionally, you can create issues on Bitbucket to track the development of this class assignment. They could represent software features, story leads, user stories, etc.

For the development of this assigment, 5 issues can be created:

1. Create a new pipeline in jenkins;
2. Configure the jenkinsfile script;
3. Create a dockerfile;
4. Run the pipeline;
5. Describe the process in the readme file for this assignment;
6. Mark your repository with the tag ca5-part2.

### Create a new pipeline (Issue 1)



### Edit the jenkinsfile (Issue 2)

Create a jenkinsfile like the following:

```shell

pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId: 'devops_1201781_credentials', url: 'https://RicardoNunes230@bitbucket.org/RicardoNunes230/devops-20-21-1201781.git'
            }
        }
        
        stage('Assemble') {
            steps {
               dir('ca5/part2/gradle-tut-basic/') {
                    echo 'Assembling...'
					sh 'chmod +x ./gradlew'
                    sh './gradlew clean bootJar'
					sh './gradlew clean bootWar'
                }
            }      
        }
		
		stage('Archiving') {
			steps {
				dir('ca5/part2/gradle-tut-basic/') {
					echo 'Archiving...'
					archiveArtifacts 'build/libs/*'
				}
			}     
		}
		
		stage('Test') {
            steps {
				dir('ca5/part2/gradle-tut-basic/') {
                    echo 'Testing...'
					script{
						if (isUnix()){
							sh './gradlew test'
							junit '**/TEST-com.greglturnquist.payroll.EmployeeTest.xml'
						}
						else{
							bat './gradlew test'
							junit '**/TEST-com.greglturnquist.payroll.EmployeeTest.xml'
						}
					}
                }
            }      
        }

		stage('Javadoc') {
			steps {
				dir('ca5/part2/gradle-tut-basic/') {
						echo 'Javadocs...'
						sh './gradlew javadoc'
				publishHTML (target: [
						allowMissing: false,
						alwaysLinkToLastBuild: false,
						keepAll: true,
						reportDir: 'build/docs/javadoc',
						reportFiles: 'index-all.html',
						reportName: "HTML Javadoc Report"
					])

				}    
			}
		}      

		stage('Publish image') {
			steps {
				dir('ca5/part2/') {    		
					script {
						echo 'Building the image...'
						
						def dockerImage = docker.build("ricardonunes230/devops_2021_1201781:${env.BUILD_NUMBER}")
						echo "build number: ${env.BUILD_NUMBER}"
						
						docker.withRegistry('https://registry.hub.docker.com', 'Dockerhub'){
							echo 'Pushing the image...'
							dockerImage.push()
						}
						
						echo 'Deleting image locally'
						sh "docker rmi ricardonunes230/devops_2021_1201781:${env.BUILD_NUMBER}"
					}
				}
			}
		}
	}
}

```

This jenkinsfile should be placed where you declared your pipeline jenkinsfile path.

This will create 6 stages:
1. Checkout - Will checkout to your git repository using the given credentials;
2. Assemble - Will assemble a clean build of the project;
3. Archiving - Will archive the generated artifact;
4. Test - Will execute clean tests and save its report;
5. Javadoc -  Will generate the javadoc of the project and publish it in Jenkins;
6. Publish image - Will generate a docker image with Tomcat and the war file and publish it in the Docker Hub.

### Create a dockerfile (Issue 3)

```shell

FROM tomcat

RUN apt-get update -y

RUN apt-get install -f

RUN apt-get install git -y

RUN apt-get install nodejs -y

RUN apt-get install npm -y

RUN mkdir -p /tmp/build

ADD gradle-tut-basic/build/libs/gradle-tut-basic-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

EXPOSE 8080

```

This dockerfile will build a docker image for this project and expose the port 8080.

### Mark your repository with the tag ca5-part2 (Issue 6)

After finishing the assignment and completing the README file, the repository should be market with a new tag "ca5-part2".

We can do this using the following **Git** commands:

```shell
git tag -a ca5-part2 -m "End of ca5-part2 assignment"

git push origin ca5-part2
```